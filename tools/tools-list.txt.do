redo-always

for path in */*/create */*/apply; do
	[ -x "$path" ] || continue

	verb=$(basename "$path")
	tool_format=$(dirname "$path")
	format=$(basename "$tool_format")
	tool=$(dirname "$tool_format")

	printf "%s\t%s\t%s\n" "$verb" "$format" "$tool"
done | sort > "$3"

redo-stamp < "$3"
