redo-ifchange tools-list.txt

cat tools-list.txt |while read VERB FORMAT TOOL; do
	[ "$VERB" = "apply" ] || continue
	[ "$FORMAT" = "$2" ] || continue

	printf "%s\n" "$TOOL" >> "$3"
done

redo-stamp < "$3"
