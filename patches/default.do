test=${2%%.*}
tool_format=${2#*.}
tool=${tool_format%%.*}
format_extra=${tool_format#*.}
format=${format_extra%%.*}

# Some tools get cranky if the patch file doesn't end with the expected extension.
patch=$(mktemp "$PWD"/XXXXXXXX."$format")

redo-ifchange \
	../tools/"$tool"/"$format"/create \
	../tests/"$test"/source \
	../tests/"$test"/target

../tools/"$tool"/"$format"/create \
	../tests/"$test"/source \
	../tests/"$test"/target \
	"$patch"

mv "$patch" "$3"
