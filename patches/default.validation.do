test=${2%%.*}
tool_format=${2#*.}
tool=${tool_format%%.*}
format_extra=${tool_format#*.}
format=${format_extra%%.*}

hashof () {
	sha512sum "$1" | grep -o "^[0-9A-Fa-f]*"
}

redo-ifchange "$2" ../tests/"$test"/source ../tests/"$test"/target

expected_hash=$(hashof ../tests/"$test"/target)

redo-ifchange ../tools/"$format".validators.txt
cat ../tools/"$format".validators.txt | while read apply_tool; do
	newtarget=$(mktemp "$PWD"/XXXXXXXX.target)

	echo "Testing with $apply_tool..." >&2

	redo-ifchange ../tools/"$apply_tool"/"$format"/apply
	../tools/"$apply_tool"/"$format"/apply ../tests/"$test"/source "$newtarget" "$2" < /dev/null

	if [ "$(hashof "$newtarget")" != "$expected_hash" ]; then
		echo "Patch application failed!" >&2
		exit 1
	fi

	echo "$apply_tool"

        rm "$newtarget"
done

