redo-ifchange \
        ../tools/tools-list.txt \
        ../tests/test-list.txt

cat ../tests/test-list.txt |
while read -r TESTNAME; do
	grep '^create' ../tools/tools-list.txt | cut -f2,3 |
        while read -r FORMAT TOOL ; do
	        printf "%s.%s.%s\n" "$TESTNAME" "$TOOL" "$FORMAT"
	        printf "%s.%s.%s.gz\n" "$TESTNAME" "$TOOL" "$FORMAT"
        done
done
