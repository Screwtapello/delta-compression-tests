# Make sure we know what patches we want to report on.
redo-ifchange \
        patches/patch-list.txt

# Build all the patches we want to report on.
(
        cd patches
        cat patch-list.txt | sed -ne 'p;s/$/.validation/;p' | xargs redo-ifchange
)

# Print a header for our results file.
printf "%s\t%s\t%s\n" "byte size" "test name" "patch type"

# Now, nicely format the names and sizes of all these patches.
cat patches/patch-list.txt | while read -r PATCHNAME; do
        TESTNAME=${PATCHNAME%%.*}
        PATCHTYPE=${PATCHNAME#*.}
        FILESIZE=$(stat -c %s "patches/$PATCHNAME")
        printf "%-10d\t%s\t%s\n" "$FILESIZE" "$TESTNAME" "$PATCHTYPE"
done
