/* file exfile.c
 * extracts a block of data from a given file
 * parameters:
 * input file
 * beginning of the data
 * size of the data
 */

#include <stdlib.h>
#include <stdio.h>

int main(int argc, char** argv){
   FILE *input;
   long offset;
   size_t size;
   unsigned char  *buf;

   if(argc<3){
      printf("usage: <input file> <offset> <size>\n");
      exit(0);
   }

   input = fopen(argv[1],"rb");
   if(input == NULL){
      fprintf(stderr, "can't open file %s\n",argv[1]);
      exit(0); 
   }

   /* get teh offset and the size */
   offset = (long) atoi(argv[2]);
   size = (size_t) atoi(argv[3]); 
  
   fprintf(stderr,"offset: %lu size: %zu \n",offset, size);

   /* allocate buffer space */
   buf = malloc(sizeof(unsigned char) * (size + 1));
   if(buf == NULL){
       printf("memory error!\n");
       exit(0);
   }
 
       
   /* move to the offset */
   fseek(input, offset, SEEK_SET);

   /* get the data */
   offset = fread((void*)buf, sizeof(unsigned char), size, input);

   if(offset != (long) size){
      fprintf(stderr, "incorrect offset/size data\n");
      exit(0); 
   }
   buf[size] = 0;

   /* output the data to the standard output */
   fwrite(buf,sizeof(unsigned char), size, stdout);
   
   fclose(input);
   exit(0);
}
