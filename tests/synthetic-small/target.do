#!/usr/bin/env python3

import sys
import os
import os.path
import array
import random

source_file = os.path.join(os.path.dirname(sys.argv[1]), "source")

os.spawnlp(os.P_WAIT, "redo-ifchange", "redo-ifchange", source_file)

handle = open(source_file, 'rb')
try:
	source = handle.read()
finally:
	handle.close()

target = array.array('b', source)

random.seed(31337)

for _ in range(100):
	source_offset = random.randrange(0, len(target)//2)
	source_length = random.randrange(17, len(target)//2)
	target_offset = random.randrange(0, len(target)-source_length)

	target[target_offset:target_offset+source_length] = \
		target[source_offset:source_offset+source_length]

handle = open(sys.argv[3], 'wb')
try:
	target.tofile(handle)
finally:
	handle.close()
